#!/usr/bin/env sh

# generic
alias poweroff="systemctl poweroff"
alias reboot="systemctl reboot"
alias v="nvim"
alias sv="sudo nvim"
alias nv="nvim"
alias snv="sudo nvim"

# xorg-related
# update xrdb with current Xresources definitions
alias xup="xrdb -merge ~/.Xresources"

# python specific
alias venv="source $(whereis virtualenvwrapper | awk '{print $2}')"

# config alias from hacker news
alias config="git --git-dir=$HOME/.dotfiles --work-tree=$HOME"

# git aliases
alias gs="git status"
alias gp="git push"
alias gpl="git pull"
alias gco="git checkout"
alias grb="git branch -d"
alias ga="git add"
alias gaa="git add ."

# navigation 'bookmarks'
alias gtproj="cd ~/projects"
alias gtconf="cd ~/.config"

# edit common configs
alias exr="v ~/.Xresources"
alias evim="v ~/.vimrc"
alias envim="v ~/.config/nvim/init.vim"
alias ezsh="v ~/.zshrc"
alias esxhkd="v ~/.config/sxhkd/sxhkdrc"
alias ebspwm="v ~/.config/bspwm/bspwmrc"
alias edunst="v ~/.config/dunst/dunstrc"
alias epolytop="v ~/.config/polybar/top.ini"
alias epolybot="v ~/.config/polybar/bottom.ini"
alias epolycol="v ~/.config/polybar/colors.ini"
alias epolyuser="v ~/.config/polybar/user_modules.ini"
alias epolysys="v ~/.config/polybar/system_modules.ini"
